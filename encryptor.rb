require 'openssl'

def encrypt(data, key, iv)
  cipher = OpenSSL::Cipher.new('AES-256-CBC')
  cipher.encrypt
  cipher.key = key
  cipher.iv = iv
  encrypted = cipher.update(data) + cipher.final
  return encrypted
end

def decrypt(encrypted_data, key, iv)
  decipher = OpenSSL::Cipher.new('AES-256-CBC')
  decipher.decrypt
  decipher.key = key
  decipher.iv = iv
  decrypted = decipher.update(encrypted_data) + decipher.final
  return decrypted
end

# Exemple d'utilisation
data_to_encrypt = "Hello, World!"
key = OpenSSL::Random.random_bytes(32)  # 256 bits for AES-256
iv = OpenSSL::Random.random_bytes(16)   # 128 bits for AES

encrypted_data = encrypt(data_to_encrypt, key, iv)
puts "Données chiffrées : #{encrypted_data}"

decrypted_data = decrypt(encrypted_data, key, iv)
puts "Données déchiffrées : #{decrypted_data}"
